import React from "react";
import Avatar from "../../../../asset/images/clientlist/avater.png";
const CustomersForm = ({ customer, index, register, errors }) => {
  return (
    <div>
      <div className="grid gap-4 xl:grid-cols-5 lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
        <div className="common_input_wraper">
          <label>Customer’s Name </label>
          <input
            {...register(`customer_name${index}`, { required: true })}
            type="text"
            placeholder="Customer’s Name "
          />
          {errors[`customer_name${index}`]?.type === "required" && (
            <small className="   text-red-700">
              *Customer Name is required
            </small>
          )}
        </div>
        {/* <div className="common_input_wraper">
          <label>Customer ID</label>
          <input type="text" placeholder="Customer ID" />
        </div> */}
        <div className="common_input_wraper">
          <label>Father’s Name</label>
          <input
            {...register(`fathers_name${index}`, { required: true })}
            type="text"
            placeholder="Father’s Name"
          />
          {errors[`fathers_name${index}`]?.type === "required" && (
            <small className="   text-red-700">
              *Father's Name is required
            </small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Mother’s Name</label>
          <input {...register(`mothers_name${index}`, { required: true })} type="text" placeholder="Mother’s Name" />
          {errors[`mothers_name${index}`]?.type === "required" && (
            <small className="   text-red-700">*Mothers Name is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Husband’s / Wife’s Name</label>
          <input {...register(`husband_or_wife_name${index}`, { required: true })} type="text" placeholder="Husband’s / Wife’s  Name" />
          {errors[`husband_or_wife_name${index}`]?.type === "required" && (
            <small className="   text-red-700">*Husband's/wife's Name is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>National ID Number</label>
          <input {...register(`national_id${index}`, { required: true })} type="text" placeholder="National ID Number" />
          {errors[`national_id${index}`]?.type === "required" && (
            <small className="   text-red-700">*National Id is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Date of Birth</label>
          <input {...register(`date_of_birth${index}`, { required: true })} type="date" />
          {errors[`date_of_birth${index}`]?.type === "required" && (
            <small className="   text-red-700">*Date of birth is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Customer Mobile No</label>
          <input {...register(`customer_mobile${index}`, { required: true })} type="text" placeholder="Customer Mobile No" />
          {errors[`customer_mobile${index}`]?.type === "required" && (
            <small className="   text-red-700">*Customer Mobile is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Others File No.</label>
          <input {...register(`others_file_no${index}`, { required: true })} type="text" placeholder="Others File No." />
          {errors[`others_file_no${index}`]?.type === "required" && (
            <small className="   text-red-700">*Others file no is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Profession</label>
          <input {...register(`professin${index}`, { required: true })}  type="text" placeholder="Profession" />

          {errors[`professin${index}`]?.type === "required" && (
            <small className="   text-red-700">*professin Name is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Designation (optional)</label>
          <input type="text" placeholder="Designation (optional)" />
        </div>
        <div className="common_input_wraper">
          <label>Email Address</label>
          <input {...register(`email${index}`, { required: true })} type="text" placeholder="Email Address" />

          {errors[`email${index}`]?.type === "required" && (
            <small className="   text-red-700">*Email is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Mailing Address</label>
          <input {...register(`mail_address${index}`, { required: true })} type="text" placeholder="Mailing Address" />

          {errors[`mail_address${index}`]?.type === "required" && (
            <small className="   text-red-700">*Mailling address is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Permanent Address</label>
          <input {...register(`permanent_address${index}`, { required: true })} type="text" placeholder="Permanent Address" />

          {errors[`permanent_address${index}`]?.type === "required" && (
            <small className="   text-red-700">*Permanent address is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Office Address (optional)</label>
          <input {...register(`office_address${index}`)} type="text" placeholder="Office Address " />
        </div>
        <div className="common_input_wraper">
          <label>Client’s Image (PP size 450x570) </label>
          <input {...register(`image${index}`, { required: true })} type="file" placeholder="Office Address " />
        </div>
        <div className="flex items-end">
          <img 

            className="h-[60px] w-[60px] object-cover"
            src={Avatar}
            alt=""
          />
        </div>
      </div>
    </div>
  );
};

export default CustomersForm;
