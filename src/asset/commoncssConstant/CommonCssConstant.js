export const commonSearchInput="lg:w-[245px] w-[150px] lg:px-[10px] px-2 lg:py-[10px] py-1 outline-none border-[1px] border-[#38414A] rounded-[5px] text-[12px] lg:text-[16px] font-[500]"


export const addCustomerCss="bg-[#16A085] text-white text-[14px] px-[13px] py-[10px] rounded-[5px] flex items-center ml-auto"

export const dashboardBodyWraper="bg-[#FFFFFF]    rounded-[4px] lg:px-[20px] py-[30px] my-[30px]"


export const agent_info_label="text-[14px] font-[500] text-[#5F5F5F]"