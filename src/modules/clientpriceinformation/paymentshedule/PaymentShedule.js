import React from "react";
import { useNavigate } from "react-router-dom";
import LeftArrow from "../../../asset/images/clientlist/left-arrow.png";
import "./paymentShedule.css";
import CommonInputWithDate from "../../../shared/CommonInputWithDate";
import PlusImg from "../../../asset/images/clientpaymentinformation/plus.png";
const PaymentShedule = () => {
  const navigate = useNavigate();
  const handleChange = (e) => {
    e.preventDefault();
    console.log(e.target.value);
  };
  return (
    <div>
      <h1 class="text-[#333547] flex items-center font-[700] text-[24px]">
        <span onClick={() => navigate(-1)} className="mr-3 cursor-pointer">
          <img src={LeftArrow} alt="" />
        </span>{" "}
        Payment Schedule
      </h1>
      <div class="bg-[#FFFFFF]   rounded-[4px] px-[20px] py-[30px] my-[30px]">
        <h1 className="shedule_title underline">Update Payment Schedule</h1>
        <button className="psi_btn">Previously Submitted Informations</button>
        <div className="psi_customer_box">
          <div className="grid xl:grid-cols-4 gap-5">
            <div class="common_input_wraper">
              <label>Customer Name</label>
              <input
                disabled
                name="file_no"
                type="text"
                placeholder="File No."
              />
            </div>
            <div class="common_input_wraper">
              <label>Customer ID</label>
              <input
                disabled
                name="file_no"
                type="text"
                placeholder="12Xsv5A"
              />
            </div>
          </div>
          <div className="grid xl:grid-cols-8 gap-5 mt-5">
            <div class="common_input_wraper">
              <label>Project Name</label>
              <input
                disabled
                name="file_no"
                type="text"
                placeholder="Munshiganj "
              />
            </div>
            <div class="common_input_wraper">
              <label>File / Plot No.</label>
              <input disabled name="file_no" type="text" placeholder="12A" />
            </div>
            <div class="common_input_wraper">
              <label>Land Size</label>
              <input
                disabled
                name="file_no"
                type="text"
                placeholder="1091 sqft"
              />
            </div>
            <div class="common_input_wraper">
              <label>Total Price</label>
              <input disabled name="file_no" type="text" placeholder="500000" />
            </div>
            <div class="common_input_wraper">
              <label>Booking Money Amount</label>
              <input disabled name="file_no" type="text" placeholder="100000" />
            </div>
            <div class="common_input_wraper">
              <label>Down payment Amount</label>
              <input disabled name="file_no" type="text" placeholder="100000" />
            </div>
          </div>
          <div className="grid xl:grid-cols-8 gap-5 mt-5">
            <div class="common_input_wraper">
              <label>Instalment Amount</label>
              <input
                disabled
                name="file_no"
                type="text"
                placeholder="Munshiganj "
              />
            </div>
            <div class="common_input_wraper">
              <label>No of Instalment </label>
              <input disabled name="file_no" type="text" placeholder="5" />
            </div>
            <div class="common_input_wraper">
              <label>Per Month Instalment </label>
              <input disabled name="file_no" type="text" placeholder="20000" />
            </div>
            <div class="common_input_wraper">
              <label>Approx. Registration Date </label>
              <input
                disabled
                name="file_no"
                type="text"
                placeholder="10/07/23"
              />
            </div>
          </div>
        </div>
        <div className="grid xl:grid-cols-3 gap-5">
          <div className="instalment_money_box mt-5">
            <p className="imb_top_title">Booking Money Information</p>
            <CommonInputWithDate
              onChange={handleChange}
              label={"Booking Money"}
            />
          </div>
          <div className="instalment_money_box mt-5">
            <p className="imb_top_title">Booking Money Information</p>
            <CommonInputWithDate
              onChange={handleChange}
              label={"Downpayment 1"}
            />
            <CommonInputWithDate
              onChange={handleChange}
              label={"Downpayment 1"}
            />
            <CommonInputWithDate
              onChange={handleChange}
              label={"Downpayment 1"}
            />

            <p className="plus_img_wraper">
              {" "}
              <img src={PlusImg} alt="" />
            </p>
          </div>
          <div className="instalment_money_box mt-5">
            <p className="imb_top_title">Booking Money Information</p>
            <CommonInputWithDate
              onChange={handleChange}
              label={"Installment 1"}
            />
            <CommonInputWithDate
              onChange={handleChange}
              label={"Installment 1"}
            />
            <CommonInputWithDate
              onChange={handleChange}
              label={"Installment 1"}
            />
            <CommonInputWithDate
              onChange={handleChange}
              label={"Installment 1"}
            />
            <CommonInputWithDate
              onChange={handleChange}
              label={"Installment 1"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaymentShedule;
