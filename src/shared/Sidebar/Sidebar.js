import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import sideBarData from "../../asset/constant/sidebar";
import "./sidebar.css";
const Sidebar = () => {
  const [active, setIsActive] = useState(null);
  const navigate = useNavigate();
  console.log(active, "active");
  return (
    <div className="drawer-side lg:bg-[#533198]">
      <div class="bg-[#FFFFFF] sticky top-0  z-50  items-center px-4 h-[80px] flex   text-[20px] text-[#000000] font-[700]">
        <span>Munshiganj Land Project</span>
      </div>
      <label htmlFor="my-drawer-2" className="drawer-overlay"></label>
      <ul className="menu p-4 w-80 h-full sidebar overflow-scroll  text-white  font-[400] text-[16px]">
        {sideBarData.map((item, i) => (
          <>
            <li
              onClick={() => {
                localStorage.setItem("title", item.title);
                if (active === i) {
                  setIsActive(null);
                } else {
                  setIsActive(i);
                  if (item?.path) {
                    navigate(item?.path);
                  }
                }
              }}
              className={`hover:bg-white   hover:rounded-[5px] ${
                localStorage.getItem("title") === item.title
                  ? "bg-white text-black rounded-[5px]"
                  : ""
              }`}
            >
              <Link
                className={`py-[14px] active:text-white ${
                  !item?.children?.length > 0
                    ? "focus:bg-white"
                    : "focus:bg-none"
                }`}
                style={{ gridAutoColumns: "initial" }}
              >
                {i + 1}.{item.title}
              </Link>
            </li>

            {item?.children?.length > 0 && (
              <ul className={`ml-3 ${active !== i ? "hide" : "show"}`}>
                {item?.children?.map((child, j) => (
                  <li
                    onClick={() => localStorage.setItem("title", child.title)}
                    className={`py-0      hover:bg-white hover:rounded-[5px] ${
                      localStorage.getItem("title") === child.title
                        ? "bg-white text-black rounded-[5px]"
                        : ""
                    }`}
                  >
                    <Link
                      to={child?.path}
                      className={`py-[14px] active:text-white focus:bg-white`}
                      style={{ gridAutoColumns: "initial" }}
                    >
                      {`${i + 1}.${j + 1}`}.{child.title}
                    </Link>
                  </li>
                ))}
              </ul>
            )}
          </>
        ))}
      </ul>
    </div>
  );
};

export default Sidebar;
