import React, { useState } from "react";
import { dashboardBodyWraper } from "../../../asset/commoncssConstant/CommonCssConstant";
import LeftArrow from "../../../asset/images/clientlist/left-arrow.png";
import { useNavigate } from "react-router-dom";
import LandInformationForm from "./form/LandInformationForm";
import LandPriceInformation from "./form/LandPriceInformation";
import AgentInformationForm from "./form/AgentInformationForm";
import CustomersForm from "./form/CustomersForm";
import { useForm } from "react-hook-form";
import { handleAddCustomer } from "./utils/addClientHandler";
const AddClient = () => {
  const navigate = useNavigate();
  const [customers, setCustomers] = useState([
    {
      customer_name: "",
      fathers_name: "",
      mothers_name: "",
      husband_name: "",
      national_id: "",
      date_of_birth: "",
      customer_mobile_no: "",
      others_file_no: "",
      professin: "",
      designation: "",
      email: "",
      mail_address: "",
      permanent_address: "",
      office_address: "",
      client_img: "",
    },
  ]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    const formData = new FormData();


    // for (const field in data) {
    //   if (Object.hasOwnProperty.call(data, field)) {
    //     const element = data[field];
        
    //     if (field.includes("image")) {
    //       console.log( `${field}`);
    //       formData.append("files", element[0]);
        
    //     } else {
    //       formData.append(field, element);
    //     }
       
    //   }
    // //   console.log("loop count",field);
    // }



    fetch('http://localhost:5000/',{
      method:"POST",
      headers:{
        'Content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify(data),
    })
    
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1 class="text-[#333547] flex items-center font-[700] text-[24px]">
        <span onClick={() => navigate(-1)} className="mr-3 cursor-pointer">
          <img src={LeftArrow} alt="" />
        </span>{" "}
        Add Client
      </h1>
      <div className={`${dashboardBodyWraper} py-[4px] `}>
        <LandInformationForm register={register} errors={errors} />
        <p className="text-[#6B2CEA] font-bold my-[40px] text-[16px] bg-[#F0EAFD] py-1  text-center">
          Customer Information
        </p>
        {customers.map((customer, i) => (

          <>
          <button onClick={()=>{
            const _data=[...customers]
            _data.splice(i,1)
            setCustomers(_data)
          }}>Delete</button>
            <CustomersForm
              register={register}
              errors={errors}
              customer={customer}
              key={i}
              index={i}
            />
            {customers.length > 1 ? <hr className="my-10" /> : null}
          </>
        ))}
        <button
          type="button"
          onClick={() => handleAddCustomer(setCustomers, customers)}
          className="add_customer"
        >
          Add More Customer <span className="plus">+</span>
        </button>
        <LandPriceInformation />

        <AgentInformationForm />
        <div className="text-end pb-12">
          <button type="submit" className="submit_button">
            Submit
          </button>
        </div>
      </div>
    </form>
  );
};

export default AddClient;
