import React from "react";
import CommonPrintPdfBtn from "../../shared/CommonPrintPdfBtn";
import Pagination from "../../shared/pagination/Pagination";
import BFolderImg from "../../asset/images/clientlandinformation/back-folder.png";
const LandSellerInformation = () => {
  return (
    <div>
      <h1 class="text-[#333547] font-[700] text-[24px]">Land Seller</h1>
      <div class="bg-[#FFFFFF]   rounded-[4px] px-[20px] py-[30px] my-[30px]">
        <div class="flex justify-between items-end mt-[20px]">
          <CommonPrintPdfBtn />

          <input
            type="text"
            placeholder="Search Seller Name / ID..."
            class="max-w-[245px] px-[10px] py-[10px] outline-none border-[1px] border-[#38414A] rounded-[5px] text-[16px] font-[500]"
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start">
                  Land Booking <br />
                  Date
                </th>

                <th class="text-[#00CA08] text-start">
                  File/ <br />
                  Plot No.
                </th>
                <th class="text-[#00CA08]">Project Name</th>
                <th class="text-[#00CA08] text-start">
                  Seller <br />
                  ID
                </th>
                <th class="text-[#00CA08] text-start">
                  Seller <br />
                  Name
                </th>
                <th class="text-[#00CA08]">
                  Land <br />
                  Broker ID
                </th>
                <th class="text-[#00CA08]">
                  Land Broker <br />
                  Name
                </th>
                <th class="text-[#00CA08]">
                  Land Broker <br />
                  Money
                </th>
                <th class="text-[#00CA08] text-start">
                  Land Broker <br />
                  Extra Amount
                </th>
                <th class="text-[#00CA08]">
                  Other <br />
                  Expanse
                </th>

                <th class="text-[#00CA08]">
                  Total <br />
                  Amount
                </th>
                <th class="text-[#00CA08] text-center">
                  Land Broker <br />
                  Paid Amount
                </th>
                <th class="text-[#00CA08] text-center">
                  Land Broker <br />
                  Due Amount
                </th>
                <th class="text-[#00CA08] text-center">
                  Land Broker <br />
                  Payment <br />
                  Statement
                </th>
                <th class="text-[#00CA08] text-center">
                  Other <br />
                  Document
                </th>
              </tr>
            </thead>
            <tbody>
              {[...Array(10).keys()].map((item, i) => (
                <tr>
                  <td>2022-05-21</td>
                  <td>
                    <span className="font-semibold">52A1C4</span>
                  </td>
                  <td>Munshiganj Project </td>
                  <td>
                    <p className="font-semibold">5248678</p>
                    <p className="font-semibold">5248678</p>
                    <p className="font-semibold">5248678</p>
                  </td>
                  <td>
                    <p className="font-semibold">Mr. Abul </p>
                    <p className="font-semibold">Mr. Kashem </p>
                    <p className="font-semibold">Mr. Hannan </p>
                  </td>
                  <td>
                    <span className="font-semibold">5248678</span>
                  </td>
                  <td>Mr. Abul Kashem </td>
                  <td><span className="font-bold">200000</span></td>
                  <td><span className="font-bold">200000</span></td>
                  <td><span className="font-bold">200000</span></td>
                  <td><span className="font-bold">200000</span></td>
                  <td><span className="font-bold">200000</span></td>
                  <td><span className="font-bold">200000</span></td>

                  <td>
                    <img class="mx-auto" src={BFolderImg} alt="" />
                  </td>
                  <td>
                    <img class="mx-auto" src={BFolderImg} alt="" />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <Pagination />
      </div>
    </div>
  );
};

export default LandSellerInformation;
