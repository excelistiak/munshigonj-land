import React from "react";
import Pagination from "../../../shared/pagination/Pagination";
import AddImg from "../../../asset/images/clientpaymentinformation/add.png";
import ViewImg from "../../../asset/images/clientlist/view.png";
import ActionImg from "../../../asset/images/clientlist/action.png";
const RegistryCompleteLandInformation = () => {
  return (
    <>
      <h1 class="text-[#333547] font-[700] text-[24px]">
        Registry Complete Land Information (From Land Owner)
      </h1>
      <div class="bg-[#FFFFFF]   rounded-[4px] px-[20px] py-[30px] my-[30px]">
        <button class="bg-[#16A085] text-white text-[14px] px-[13px] py-[10px] rounded-[5px] flex items-center ml-auto">
          Add Customer{" "}
          <img class="ml-[30px]" src="./image/clientlist/add-user.png" alt="" />
        </button>
        <div class="flex justify-end items-end mt-[20px]">
          <input
            type="text"
            placeholder="Select Search Type"
            class="max-w-[245px] px-[10px] py-[10px] outline-none border-[1px] border-[#38414A] rounded-[5px] text-[16px] font-[500]"
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start">
                  {" "}
                  Land Purchase/ Sub-deed Date
                </th>

                <th class="text-[#00CA08] text-start">Sub-deed Number</th>
                <th class="text-[#00CA08]">Project Name</th>
                <th class="text-[#00CA08] text-start">Mouza Name</th>
                <th class="text-[#00CA08] text-start">
                  Purchased Land Size (Decimal)
                </th>
                <th class="text-[#B7A400]">Purchased Land CS/SA Daag No</th>
                <th class="text-[#B7A400]">Purchased Land CS/SA KH No</th>
                <th class="text-[#0039CA]">Purchased Land RS Daag No</th>
                <th class="text-[#0039CA] text-start">
                  Purchased Land RS KH No
                </th>
                <th class="text-[#AE009D]">Purchased Land BS Daag No</th>

                <th class="text-[#AE009D]">Purchased Land BS KH No</th>
                <th class="text-[#00CA08] text-center">Edit / View Details</th>
                <th class="text-[#00CA08] text-center">
                  Previous Land Summery
                </th>
                <th class="text-[#00CA08] text-center">
                  Previous Land Dispute Solved by Court
                </th>
              </tr>
            </thead>
            <tbody>
              {[...Array(20).keys()].map((item, i) => (
                <tr>
                  <td>dd-mm-yy</td>
                  <td className="font-[700]">521073</td>
                  <td>Munshiganj Project</td>
                  <td>Mouza Name</td>
                  <td className="font-[700]">521073</td>
                  <td className="font-[700]">521073</td>
                  <td className="font-[700]">521073</td>
                  <td className="font-[700]">521073</td>
                  <td className="font-[700]">521073</td>
                  <td className="font-[700]">521073</td>
                  <td className="font-[700]">521073</td>
                  <td class="w-[230px]">
                    <div class="h-full flex">
                    <img
                        class="mr-2"
                        src={ActionImg}
                        alt=""
                      />
                      <img
                        class="mr-2"
                        src={ViewImg}
                        alt=""
                      />
                     
                    </div>
                  </td>
                  <td class="w-[230px]">
                    <div class="h-full flex">
                    <img
                        class="mr-2"
                        src={AddImg}
                        alt=""
                      />
                      <img
                        class="mr-2"
                        src={ViewImg}
                        alt=""
                      />
                     
                    </div>
                  </td>
                  <td class="w-[230px]">
                    <div class="h-full flex">
                    <img
                        class="mr-2"
                        src={AddImg}
                        alt=""
                      />
                      <img
                        class="mr-2"
                        src={ViewImg}
                        alt=""
                      />
                     
                    </div>
                  </td>
                
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <Pagination />
      </div>
    </>
  );
};

export default RegistryCompleteLandInformation;
