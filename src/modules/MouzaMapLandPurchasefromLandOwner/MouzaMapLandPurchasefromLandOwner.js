import React from "react";
import Pagination from "../../shared/pagination/Pagination";
import BFolderImg from "../../asset/images/clientlandinformation/back-folder.png";
import ActionImg from "../../asset/images/clientlist/action.png";
import AddImage from "../../asset/images/clientpaymentinformation/add.png";
const MouzaMapLandPurchasefromLandOwner = () => {
  return (
    <div>
      <h1 class="text-[#333547] font-[700] text-[24px]">
        Mouza Map (Land Purchase from Land Owner)
      </h1>
      <div class="bg-[#FFFFFF]   rounded-[4px] px-[20px] py-[30px] my-[30px]">
        <div class="flex justify-end items-end mt-[20px]">
          {/* <CommonPrintPdfBtn /> */}

          <input
            type="text"
            placeholder="Select Search Type"
            class="max-w-[245px] px-[10px] py-[10px] outline-none border-[1px] border-[#38414A] rounded-[5px] text-[16px] font-[500]"
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start">Project Name</th>

                <th class="text-[#00CA08] text-start">Mouza Name</th>
                <th class="text-[#B7A400]">
                  Mention Land Size <br />
                  at CS/SA <br />
                  Daag No
                </th>
                <th class="text-[#00CA08] text-start">
                  Purchased <br />
                  Land Size <br />
                  (Decimal)
                </th>
                <th class="text-[#00B0A6] text-start">
                  Sub-deed <br />
                  Registry <br />
                  Land Size
                </th>
                <th class="text-[#00B0A6] text-start">
                  Registry <br />
                  Bayna <br />
                  Land Size
                </th>
                <th class="text-[#00B0A6] text-start">
                  All Buy/Sell <br />
                  Registry <br />
                  Power <br />
                  Land Size
                </th>
                <th class="text-[#00B0A6] text-start">
                  Registry <br />
                  Power <br />
                  Land Size
                </th>
                <th class="text-[#CA0000]">
                  No Agreement <br />
                  Land Size
                </th>
                <th class="text-[#0039CA]">
                  Dispute <br />
                  Land <br />
                  RS Daag No
                </th>
                <th class="text-[#AE009D]">
                  Dispute <br />
                  Land <br />
                  BS Daag No
                </th>
                <th class="text-[#00CA08] text-start">
                  Dispute <br />
                  Land Size
                </th>
                <th class="text-[#00CA08]">
                  Dewani <br />
                  Mamla <br />
                  Number
                </th>

                <th class="text-[#00CA08]">
                  Dewani <br />
                  Mamla <br />
                  Date
                </th>
                <th class="text-[#00CA08] text-center">
                  Dewani Mamla <br />
                  High Court <br />
                  Apil Number
                </th>
                <th class="text-[#00CA08] text-center">
                  Add <br />
                  Details
                </th>
                <th class="text-[#00CA08] text-center">
                  Edit <br />
                  Details
                </th>
                <th class="text-[#00CA08] text-center">Map Folder</th>
              </tr>
            </thead>
            <tbody>
              {[...Array(10).keys()].map((item, i) => (
                <tr>
                  <td>Munshiganj Project</td>
                  <td>Mr. Abul</td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                  <td>
                    <span className="font-bold">521073 </span>
                  </td>
                
                
                 
                  <td>
                    <img class="mx-auto" src={AddImage} alt="" />
                  </td>
                  <td>
                    <img class="mx-auto" src={ActionImg} alt="" />
                  </td>
                  <td>
                    <img class="mx-auto" src={BFolderImg} alt="" />
                  </td>

                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <Pagination />
      </div>
    </div>
  );
};

export default MouzaMapLandPurchasefromLandOwner;
