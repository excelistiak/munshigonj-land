import React from "react";
import Pagination from "../../../shared/pagination/Pagination";
import {
  commonSearchInput,
  dashboardBodyWraper,
} from "../../../asset/commoncssConstant/CommonCssConstant";
import CommonPrintPdfBtn from "../../../shared/CommonPrintPdfBtn";

import BFolderImg from "../../../asset/images/clientlandinformation/back-folder.png";
const AgentPaymentList = () => {
  return (
    <div>
      <h1 class="text-[#333547] font-[700] text-[24px]">Agent Payment List</h1>
      <div className={dashboardBodyWraper}>
        <div class="flex justify-between  flex-wrap gap-3 items-end mt-[20px]">
          <CommonPrintPdfBtn />
          <input
            type="text"
            placeholder="Search"
            className={commonSearchInput}
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start">Client Booking Date</th>
                <th class="text-[#00CA08]">Agent Id</th>
                <th class="text-[#00CA08] text-start">Agent Name</th>
                <th class="text-[#00CA08]">
                  Customer’s <br />
                  ID
                </th>
                <th class="text-[#00CA08] text-start">
                  Customer’s <br />
                  Name
                </th>
                <th class="text-[#00CA08] text-start">Project Name</th>
                <th class="text-[#00CA08]">
                  File/ <br />
                  Plot No.
                </th>
                <th class="text-[#00CA08]">Agent Money</th>
                <th class="text-[#00CA08]">
                  Agent Extra <br />
                  Amount
                </th>
                <th class="text-[#00CA08] text-start">
                  Other <br />
                  Expanse
                </th>
                <th class="text-[#00CA08]">
                  Total <br />
                  Amount
                </th>

                <th class="text-[#00CA08]">
                  Agent Paid <br />
                  Amount
                </th>
                <th class="text-[#00CA08]">
                  Agent Due <br />
                  Amount
                </th>
                <th class="text-[#00CA08]">
                  Create <br />
                  Payment
                </th>

                <th class="text-[#00CA08] text-center">Add Payment </th>
                <th class="text-[#00CA08] text-center">
                  Agent Payment Statement
                </th>
              </tr>
            </thead>
            <tbody>
              {[...Array(14).keys()].map((item, i) => (
                <tr>
                  <td>2022-05-21</td>
                  <td>
                    <span class="font-bold">5248678</span>
                  </td>
                  <td>Mr. Abul Kashem </td>
                  <td>
                    <span class="font-bold">5248678</span>
                  </td>
                  <td>Mr. Abul </td>
                  <td>Munshiganj Project</td>
                  <td>
                    <span class="font-bold">52A1C4</span>
                  </td>
                  <td>
                    <span class="font-bold">200000</span>
                  </td>
                  <td>
                    <span class="font-bold">5000</span>
                  </td>
                  <td>
                    <span class="font-bold">10000</span>
                  </td>
                  <td>
                    <span class="font-bold">215000</span>
                  </td>
                  <td>
                    <span class="font-bold">10000</span>
                  </td>
                  <td>
                    <span class="font-bold">90000</span>
                  </td>
                  <td>
                    <button className="bg-[#6B2CEA] whitespace-nowrap  text-white  rounded-[4px] px-[10px] py-[4px] text-[12px]">
                      Create Payment
                    </button>
                  </td>
                  <td>
                    <button className="bg-[#0085FC] whitespace-nowrap  text-white  rounded-[4px] px-[10px] py-[4px] text-[12px]">
                      Add Payment
                    </button>
                  </td>

                  <td>
                    <img class="mx-auto" src={BFolderImg} alt="" />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <Pagination />
      </div>
    </div>
  );
};

export default AgentPaymentList;
