import React from "react";
import { MdArrowBackIosNew, MdArrowForwardIos } from "react-icons/md";
const Pagination = () => {
  return (
    <div class="flex flex-wrap justyfy-end lg:justify-between lg:items-center mt-[20px] lg:mt-[60px]">
      <p class="text-[16px] flex-1 text-center lg:text-start order-2 lg:order-1 font-[400]">
        Showing 1 to 10 of 735 entries
      </p>
      <div class="flex  pagination lg:order-2 gap-[5px] bg-[#F9F5FF] py-[8px] px-[20px] rounded-full">
        <span class="">
          <MdArrowBackIosNew />
        </span>
        <span class="">1</span>
        <span class="">1</span>
        <span class="">1</span>
        <span class="">1</span>
        <span class="">1</span>
        <span class="">
          <MdArrowForwardIos />
        </span>
      </div>
    </div>
  );
};

export default Pagination;
