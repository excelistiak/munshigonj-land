import { Outlet } from "react-router-dom";
import Sidebar from "../shared/Sidebar/Sidebar";

const MainLayout = () => {
  return (
    <div className="drawer lg:drawer-open ">
      <input id="my-drawer-2" type="checkbox" className="drawer-toggle" />
      <div className="drawer-content overflow-hidden ">
        <header class="col-start-2 col-span-12 sticky top-0">
          <nav class="h-[80px] bg-[#533198] overflow-hidden flex items-center">
            <h1 class="text-[#FFFFFF] md:block  hidden font-[600] px-[16px] text-[34px]">
              Munshiganj Land Project
            </h1>
          </nav>
        </header>
        <div  class=" h-[90vh] overflow-auto bg-[#F5F6F8] w-[100%] p-[17px]">
          <Outlet />
        </div>
      </div>
      <Sidebar />
    </div>
  );
};

export default MainLayout;
