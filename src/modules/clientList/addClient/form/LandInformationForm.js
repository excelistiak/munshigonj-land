import React from "react";

const LandInformationForm = ({ register, errors }) => {
  return (
    <div>
      <p className="text-[#6B2CEA] font-bold text-[16px] bg-[#F0EAFD] py-1  text-center">
        Land Information
      </p>

      <div className="grid gap-4 xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
        <div className="common_input_wraper">
          <label>File/Plot No.</label>
          <input
            {...register("file_no", { required: true })}
            type="text"
            placeholder="File No."
          />
          {errors.file_no?.type === "required" && (
            <small className="   text-red-700">*File No is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Mouza Name</label>
          <input
            {...register("mouza_name", { required: true })}
            type="text"
            placeholder="Mouza Name."
          />
          {errors.mouza_name?.type === "required" && (
            <small className="   text-red-700">*Mouza Name is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Booking Date</label>
          <input
            {...register("booking_date", { required: true })}
            type="date"
          />
          {errors.booking_date?.type === "required" && (
            <small className="   text-red-700">*Booking date is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Expected Registration Date</label>
          <input
            {...register("expected_reg_date", { required: true })}
            type="date"
          />
          {errors.expected_reg_date?.type === "required" && (
            <small className="   text-red-700">
              * Registration date is required
            </small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Project Name</label>
          <input
            {...register("project_name", { required: true })}
            type="text"
            placeholder="Building No."
          />
          {errors.project_name?.type === "required" && (
            <small className="   text-red-700">*Project Name is required</small>
          )}
        </div>
        <div className="common_input_wraper">
          <label>Land Size.</label>
          <input
            {...register("land_size", { required: true })}
            type="text"
            placeholder="Flat Size"
          />
          {errors.land_size?.type === "required" && (
            <small className="   text-red-700">*Land Size is required</small>
          )}
        </div>
      </div>
    </div>
  );
};

export default LandInformationForm;
