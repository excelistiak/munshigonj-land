import React from "react";
import LeftArrow from "../../../asset/images/clientlist/left-arrow.png";
import { useNavigate } from "react-router-dom";
import "./clientview.css";
import { agent_info_label } from "../../../asset/commoncssConstant/CommonCssConstant";
import CommonPrintPdfBtn from "../../../shared/CommonPrintPdfBtn";
import MaskGroup from "../../../asset/images/clientlist/Maskgroup.png";
const ClientView = () => {
  const navigate = useNavigate();
  return (
    <>
      <h1 class="text-[#333547] flex items-center font-[700] text-[24px]">
        <span onClick={() => navigate(-1)} className="mr-3 cursor-pointer">
          <img src={LeftArrow} alt="" />
        </span>{" "}
        View Client Information
      </h1>

      <div className="grid lg:grid-cols-12  mt-3 gap-5">
        <div className="col-span-9 order-2 lg:order-1 client_info_section p-[40px] ">
          <h1 className="info_title">Information</h1>
          <div className="grid lg:grid-cols-12">
            <div className="client_info_table order-2 lg:order-1 col-span-9 mt-3">
              <table>
                <tr>
                  <td>
                    <p>
                      Customer ID <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">570032</span>
                    </p>
                  </td>
                  <td>
                    <p>
                      Down Payment <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">
                        150000 tk
                      </span>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>
                      File / Plot No <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">52A1C4</span>
                    </p>
                  </td>
                  <td>
                    <p>
                      Booking Money <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">
                        150000 tk
                      </span>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>
                      Land Price Per Decimal <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">4200 tk</span>
                    </p>
                  </td>
                  <td>
                    <p>
                      Total Instalment amount <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">
                        150000 tk
                      </span>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>
                      Project Name <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">
                        Munshiganj Project
                      </span>
                    </p>
                  </td>
                  <td>
                    <p>
                      Total Price<span>:</span>
                      <span className="font-bold text-[#5F5F5F]">
                        10000000 tk
                      </span>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>
                      Land Size <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">
                        1091 sqft
                      </span>
                    </p>
                  </td>
                  <td>
                    <p>
                      No of Instalment <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">24</span>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <p>
                      Per Month Instalment amount <span>:</span>
                      <span className="font-bold text-[#5F5F5F]">
                        150000 tk
                      </span>
                    </p>
                  </td>
                </tr>
              </table>
            </div>
            <div className="col-span-3 lg:order-2 order-1 flex items-center">
              <span className="text-[12px] ml-3 text-center   font-[500] text-[#5F5F5F]">
                <span className="block  ">Passport Size</span>
                <span className="block  ">450x570</span>
              </span>

              <img className="ml-auto" src={MaskGroup} alt="" />
            </div>
          </div>
          <hr className="bg-[#EAEAEA] my-[40px] h-[1px] w-full" />
          <h1 className="client_info_title">Client’s Information</h1>
          <div className="customer_info_wraper">
            <div>
              <h1>Customer :1</h1>
              <div className="grid lg:grid-cols-2 gap-3">
                <div className="client_info_table_without_border">
                  <table>
                    <tr>
                      <td>Customer’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Mr. Kashem</td>
                    </tr>
                    <tr>
                      <td>Father’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Father’s Name</td>
                    </tr>
                    <tr>
                      <td>Mother’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Mother’s Name</td>
                    </tr>
                    <tr>
                      <td>Husband’s/Father’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Husband’s/Father’s Name (relationship)</td>
                    </tr>
                    <tr>
                      <td>Husband’s/Father’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Husband’s/Father’s Name (relationship)</td>
                    </tr>
                    <tr>
                      <td>Email Address</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>someone@example.com</td>
                    </tr>
                    <tr>
                      <td>Mailing Address</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>
                        18 Rochford Road, Basingstoke, Hampshire, RG21 7TQ
                      </td>
                    </tr>
                    <tr>
                      <td>Permanent Address</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>
                        18 Rochford Road, Basingstoke, Hampshire, RG21 7TQ
                        sadfas asdfas asdfas{" "}
                      </td>
                    </tr>
                    <tr>
                      <td>Mouza Names</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Mr. Kashem</td>
                    </tr>
                  </table>
                </div>
                <div className="client_info_table_without_border">
                  <table>
                    <tr>
                      <td>Date of Birth</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>dd-mm-yy</td>
                    </tr>
                    <tr>
                      <td>Phone Number</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Phone Number (1st)</td>
                    </tr>
                    <tr>
                      <td>National ID Number</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>National ID Number</td>
                    </tr>
                    <tr>
                      <td>Profession</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Profession</td>
                    </tr>
                    <tr>
                      <td>Designation (optional)</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Designation (optional)</td>
                    </tr>
                    <tr>
                      <td>Country</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Country</td>
                    </tr>
                    <tr>
                      <td>Booking Date</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Booking Date</td>
                    </tr>
                    <tr>
                      <td>Office Address (optional)</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Office Address (optional)</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
            <hr className="bg-[#EAEAEA] w-9/12 mx-auto my-[40px] h-[1px] " />
            <div>
              <h1>Customer :1</h1>
              <div className="grid lg:grid-cols-2 gap-3">
                <div className="client_info_table_without_border">
                  <table>
                    <tr>
                      <td>Customer’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Mr. Kashem</td>
                    </tr>
                    <tr>
                      <td>Father’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Father’s Name</td>
                    </tr>
                    <tr>
                      <td>Mother’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Mother’s Name</td>
                    </tr>
                    <tr>
                      <td>Husband’s/Father’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Husband’s/Father’s Name (relationship)</td>
                    </tr>
                    <tr>
                      <td>Husband’s/Father’s Name</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Husband’s/Father’s Name (relationship)</td>
                    </tr>
                    <tr>
                      <td>Email Address</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>someone@example.com</td>
                    </tr>
                    <tr>
                      <td>Mailing Address</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>
                        18 Rochford Road, Basingstoke, Hampshire, RG21 7TQ
                      </td>
                    </tr>
                    <tr>
                      <td>Permanent Address</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>
                        18 Rochford Road, Basingstoke, Hampshire, RG21 7TQ
                        sadfas asdfas asdfas{" "}
                      </td>
                    </tr>
                    <tr>
                      <td>Mouza Names</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Mr. Kashem</td>
                    </tr>
                  </table>
                </div>
                <div className="client_info_table_without_border">
                  <table>
                    <tr>
                      <td>Date of Birth</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>dd-mm-yy</td>
                    </tr>
                    <tr>
                      <td>Phone Number</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Phone Number (1st)</td>
                    </tr>
                    <tr>
                      <td>National ID Number</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>National ID Number</td>
                    </tr>
                    <tr>
                      <td>Profession</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Profession</td>
                    </tr>
                    <tr>
                      <td>Designation (optional)</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Designation (optional)</td>
                    </tr>
                    <tr>
                      <td>Country</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Country</td>
                    </tr>
                    <tr>
                      <td>Booking Date</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Booking Date</td>
                    </tr>
                    <tr>
                      <td>Office Address (optional)</td>
                      <td>
                        <span className="mx-5">:</span>
                      </td>
                      <td>Office Address (optional)</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <hr className="bg-[#EAEAEA] my-[40px] h-[1px] w-full" />

          <h1 class="client_info_title">Agent Information</h1>
          <form className="grid lg:grid-cols-2 gap-5">
            <div class="common_input_wraper grid grid-cols-2 items-center">
              <span className={agent_info_label}>Agent Name</span>
              <input name="mouza_name" type="text" placeholder="Media Name" />
            </div>
            <div class="common_input_wraper grid grid-cols-2 items-center">
              <span className={`${agent_info_label} lg:text-center`}>
                Agent Phone Number
              </span>
              <input
                name="mouza_name"
                type="text"
                placeholder="Agent Phone Number"
              />
            </div>
            <div class="common_input_wraper grid grid-cols-2 items-center">
              <span className={agent_info_label}>Other File No.</span>
              <input
                name="mouza_name"
                type="text"
                placeholder="Other File No."
              />
            </div>
            <div class="common_input_wraper grid grid-cols-2 items-center">
              <span className={`${agent_info_label} lg:text-center`}>
                Agent Money Amount
              </span>
              <input
                name="mouza_name"
                type="text"
                placeholder="Agent Money Amount"
              />
            </div>
            <div className="mt-16">
              <div className="border-t-[1px] border-[#333547] max-w-[200px]">
                <span className={`${agent_info_label} text-center block`}>
                  Customer’s Signature
                </span>
              </div>
            </div>
            <div className="mt-16">
              <div className="border-t-[1px] border-[#333547] max-w-[200px] lg:ml-auto">
                <span className={`${agent_info_label} text-center block`}>
                  Official Signature
                </span>
              </div>
            </div>
          </form>
        </div>
        <div className="col-span-3 order-1 lg:order-2  ">
          <CommonPrintPdfBtn isBlock={true} />
          <div className="commonprintpdfbtn whitespace-nowrap mt-5">
            <button>Legal Paper Folder</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ClientView;
