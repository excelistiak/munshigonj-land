const sideBarData = [
  {
    path: "/",
    title: "Dashboard",
    children: [],
  },
  {
    path: "/dashboard/client-list",
    title: "Client List",
    children: [],
  },
 
  {
    path: "/dashboard/client-price-information",
    title: "Client Price Information",
    children: [],
  },
  {
    path: "/dashboard/client-payment-information",
    title: "Client Payment Information",
    children: [],
  },
  {
    path: "/dashboard/client-land-description",
    title: "Client Land Description",
    children: [],
  },
  {
    title: "Project Land Details",
    children: [
      {
        path: "/dashboard/registry-complete-land-information",
        title: "Registry Complete Land Information",
      },
      {
        path: "",
        title: "Full Power of eternity Complete",
      },
      {
        path: "",
        title: "Part Power  of eternity ",
      },
      {
        path: "",
        title: "Registered Bayna Land Description ",
      },
      {
        path: "",
        title: "Unregistered Bayna Land Description "
      },
    ],
  },
  {
    path: "/dashboard/land-seller-information",
    title: "Land Seller Information",
    // children: [],
  },
  {
    path: "/dashboard/land-seller-payment-information",
    title: "Land Seller Payment Information",
    // children: [],
  },
  {
    path: "/dashboard/mouzamap-landpurchase-fromlandowner",
    title: "Mouza Map (Land Purchase  from Land Owner)",
    children: [],
  },
  {
    path: "/dashboard/mouzamap-landsell-tocustomer",
    title: "Mouza Map (Land Sell to Customer)",
    children: [],
  },
  
  {
    title: "Agent Information",
    children:  [
      {
        path: "/dashboard/agent-information/agent-list",
        title: "Agent List",
      },
      {
        path: "/dashboard/agent-information/agent-work-list",
        title: "Agent Work List",
      },
      {
        path: "/dashboard/agent-information/agent-payment-list",
        title: "Agent Payment List",
      }
    ],
  },
  {
    title: "Land Broker Information",
    children:  [
      {
        path: "/dashboard/agent-information/agent-list",
        title: "Land Broker List",
      },
      {
        path: "/dashboard/agent-information/agent-work-list",
        title: "Land Broker Work List",
      },
      {
        path: "/dashboard/agent-information/agent-payment-list",
        title: "Land Broker Payment List",
      }
    ],
  },
];

export default sideBarData;
