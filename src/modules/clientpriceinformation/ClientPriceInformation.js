import React from "react";
import Pagination from "../../shared/pagination/Pagination";
import CommonPrintPdfBtn from "../../shared/CommonPrintPdfBtn";
import ViewImg from "../../asset/images/clientlist/view.png";
import ActionImg from "../../asset/images/clientlist/action.png";
import { useNavigate } from "react-router-dom";

const ClientPriceInformation = () => {
  const navigate=useNavigate()
  return (
    <>
      <h1 class="text-[#333547] font-[700] text-[24px]">
        Client Price Information{" "}
      </h1>
      <div class="bg-[#FFFFFF]   rounded-[4px] px-[20px] py-[30px] my-[30px]">
        <div class="flex justify-between items-end mt-[20px]">
          <CommonPrintPdfBtn />

          <input
            type="text"
            placeholder="Search"
            class="max-w-[245px] px-[10px] py-[10px] outline-none border-[1px] border-[#38414A] rounded-[5px] text-[16px] font-[500]"
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start"> Booking Date</th>

                <th class="text-[#00CA08] text-start">Customer’s Name</th>
                <th class="text-[#00CA08]">Customer’s Id</th>
                <th class="text-[#00CA08] text-start">Project Name</th>
                <th class="text-[#00CA08] text-start">File / Plot No</th>
                <th class="text-[#00CA08]">Land Size</th>
                <th class="text-[#00CA08]">Booking Money</th>
                <th class="text-[#00CA08]">Down payment</th>
                <th class="text-[#00CA08] text-start">
                  Total instalment amount
                </th>
                <th class="text-[#00CA08]">Total Land Price</th>

                <th class="text-[#00CA08]">No of instalment </th>
                <th class="text-[#00CA08] text-center">
                  par instalment amount{" "}
                </th>
                <th class="text-[#00CA08] text-center">Agent Name</th>
                <th class="text-[#00CA08] text-center">Agent ph no</th>
                <th class="text-[#00CA08] text-center">Agent Money</th>
                <th class="text-[#00CA08] text-center">
                  Expected Registration Date
                </th>
                <th class="text-[#00CA08] text-center">Update/ View</th>
                <th class="text-[#00CA08] text-center">Schedule Date Update</th>
                <th class="text-[#00CA08] text-center">
                  View Payment Schedule
                </th>
              </tr>
            </thead>
            <tbody>
              {[...Array(20).keys()].map((item, i) => (
                <tr>
                  <td>2022-05-21</td>
                  <td>Mr. Abul </td>
                  <td>5248678</td>
                  <td>Munshiganj Project</td>
                  <td>52A1C4</td>
                  <td>1091</td>
                  <td>5,0000 TK</td>
                  <td>5,0000 TK</td>
                  <td>5,00,000 TK</td>
                  <td>5,00,000 TK</td>
                  <td>24</td>
                  <td>60000</td>
                  <td>Md. Kamrul</td>
                  <td>01234567891</td>
                  <td>60000</td>
                  <td>dd-mm-yy</td>
                  <td>
                    <img class="mx-auto" src={ActionImg} alt="" />
                  </td>
                  <td>
                    <button onClick={()=>navigate('/dashboard/client-price-information/payment-shedule/:id')} class="text-white inline-block bg-[#6B2CEA] text-[12px] font-[700] px-[25px] py-[8px] rounded-[5px]">
                      Update
                    </button>
                  </td>
                  <td>
                    <img class="mx-auto" src={ViewImg} alt="" />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <Pagination />
      </div>
    </>
  );
};

export default ClientPriceInformation;
