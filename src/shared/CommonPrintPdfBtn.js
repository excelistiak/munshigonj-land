import React from "react";

const CommonPrintPdfBtn = ({isBlock}) => {
  return (
    <div className="commonprintpdfbtn whitespace-nowrap">
      <button className={`${isBlock?"lg:block mb-3":""}`}>Copy</button>
      <button className={`${isBlock?"lg:block mb-3":""}`}>Print</button>
      <button className={`${isBlock?"lg:block mb-3":""}`}>PDF</button>
    </div>
  );
};

export default CommonPrintPdfBtn;
