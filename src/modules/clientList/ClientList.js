import React from "react";
import ActionBtn from "../../asset/images/clientlist/action.png";
import ViewBtn from "../../asset/images/clientlist/view.png";
import Pagination from "../../shared/pagination/Pagination";
import CommonPrintPdfBtn from "../../shared/CommonPrintPdfBtn";
import { addCustomerCss, commonSearchInput, dashboardBodyWraper } from "../../asset/commoncssConstant/CommonCssConstant";
import {  useNavigate } from "react-router-dom";

const ClientList = () => {
 const navigate=useNavigate()
  return (
    <div>
      <h1 class="text-[#333547] font-[700] text-[24px]">Client Information</h1>
      <div className={dashboardBodyWraper}>
        <button onClick={()=>navigate('add-client')}  className={addCustomerCss}>
          Add Customer{" "}
          <img class="ml-[30px]" src="./image/clientlist/add-user.png" alt="" />
        </button>
        <div class="flex justify-between  flex-wrap gap-3 items-end mt-[20px]">
          <CommonPrintPdfBtn />
          <input
            type="text"
            placeholder="Search"
            className={commonSearchInput}
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start">Customer Booking Date</th>
                <th class="text-[#00CA08]">Mouza Name</th>
                <th class="text-[#00CA08] text-start">Customer’s Name</th>
                <th class="text-[#00CA08]">Customer’s Id</th>
                <th class="text-[#00CA08] text-start">
                  Customer’s Permanent Address
                </th>
                <th class="text-[#00CA08] text-start">Customer’s mobile no.</th>
                <th class="text-[#00CA08]">Project Name</th>
                <th class="text-[#00CA08]">File/Plot No.</th>
                <th class="text-[#00CA08]">Land Size</th>
                <th class="text-[#00CA08] text-start">
                  Land Price (per decimal)
                </th>
                <th class="text-[#00CA08]">Total Price</th>

                <th class="text-[#00CA08]">Agent man Name</th>
                <th class="text-[#00CA08] text-center">
                  Action Update/ Delete
                </th>
                <th class="text-[#00CA08] text-center">View</th>
              </tr>
            </thead>
            <tbody>
              {[...Array(14).keys()].map((item, i) => (
                <tr>
                  <td>2022-05-21</td>
                  <td>Bangladesh</td>
                  <td>Mr. Abul </td>
                  <td>
                    <span class="font-bold">5248678</span>
                  </td>
                  <td>111/C Moghbazar, Dhaka 1217</td>
                  <td>
                    <span class="text-[#0085FC] underline">01756324158</span>
                  </td>
                  <td>
                    Munshiganj <br /> Project
                  </td>
                  <td>
                    <span class="font-bold">52A1C4</span>
                  </td>
                  <td>
                    <span class="font-bold">1091</span>
                  </td>
                  <td>5,00,000 TK</td>
                  <td>
                    <span class="font-bold">1091</span>
                  </td>
                  <td>MD . Rakib Hasan</td>
                  <td>
                    <img onClick={()=>navigate('add-client')}  class="mx-auto" src={ActionBtn} alt="" />
                  </td>
                  <td>
                    <img onClick={()=>navigate(`/dashboard/client-list/view/:id`)} class="mx-auto" src={ViewBtn} alt="" />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <Pagination />
      </div>
    </div>
  );
};

export default ClientList;
