import React from "react";

const AgentInformationForm = () => {
  return (
    <div>
      <p className="text-[#6B2CEA] font-bold text-[16px] bg-[#F0EAFD] py-1 my-[40px]  text-center">
        Agent Information
      </p>

      <div className="grid gap-4 xl:grid-cols-5 lg:grid-cols-3 md:grid-cols-2 grid-cols-1 pb-16">
        <div className="common_input_wraper">
          <label>Agent ID</label>
          <input type="text" placeholder="Media Man ID" />
        </div>
        <div className="common_input_wraper">
          <label>Agent Name</label>
          <input type="text" placeholder="Agent Name" />
        </div>
        <div className="common_input_wraper">
          <label>Agent Phone Number</label>
          <input type="text" placeholder="Agent Phone Number" />
        </div>
        <div className="common_input_wraper">
          <label>Agent Money Amount</label>
          <input type="text" placeholder="Agent Money Amount" />
        </div>
        <div className="common_input_wraper">
          <label>Agreement Upload (Multiple) </label>
          <input type="text" placeholder="Agreement Upload (Multiple)" />
        </div>
      </div>
    </div>
  );
};

export default AgentInformationForm;
