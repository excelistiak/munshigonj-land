import React from "react";
import Pagination from "../../shared/pagination/Pagination";
import CommonPrintPdfBtn from "../../shared/CommonPrintPdfBtn";
import AddImage from '../../asset/images/clientpaymentinformation/add.png'
import FolderImg from '../../asset/images/clientpaymentinformation/foldertwo.png'
import ViewImg from '../../asset/images/clientlist/view.png'
import ActionImg from '../../asset/images/clientlist/action.png'
const ClientPaymentInformation = () => {
  return (
    <div>
      <h1 class="text-[#333547] font-[700] text-[24px]">
        Client Payment List{" "}
      </h1>
      <div class="bg-[#FFFFFF]   rounded-[4px] px-[20px] py-[30px] my-[30px]">
        <div class="flex justify-between items-end mt-[20px]">
          <CommonPrintPdfBtn />

          <input
            type="text"
            placeholder="Search"
            class="max-w-[245px] px-[10px] py-[10px] outline-none border-[1px] border-[#38414A] rounded-[5px] text-[16px] font-[500]"
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start">
                  Customer’s Booking Date
                </th>

                <th class="text-[#00CA08] text-start">Customer’s Name</th>
                <th class="text-[#00CA08]">Customer’s Id</th>
                <th class="text-[#00CA08] text-start">File / Plot Number</th>
                <th class="text-[#00CA08] text-start">Project Name</th>
                <th class="text-[#00CA08]">Booking Money</th>
                <th class="text-[#00CA08]">Booking Money Payment Complete</th>
                <th class="text-[#00CA08]">Due Booking Money Payment</th>
                <th class="text-[#00CA08] text-start">Downpayment Amount</th>
                <th class="text-[#00CA08]">Downpayment Complete Amount</th>

                <th class="text-[#00CA08]">Due Downpayment</th>
                <th class="text-[#00CA08] text-center">
                  Total Instalment Amount
                </th>
                <th class="text-[#00CA08] text-center">
                  Instalment Payment Complete Amount
                </th>
                <th class="text-[#00CA08] text-center">
                  Due Instalment Payment{" "}
                </th>
                <th class="text-[#00CA08] text-center">Total Price</th>
                <th class="text-[#00CA08] text-center">Total Payment Amount</th>
                <th class="text-[#00CA08] text-center">Total Due</th>
                <th
                  style={{ width: "140px" }}
                  class="text-[#00CA08]  text-center"
                >
                  Action Add/Edit Payment Statement Details
                </th>
                <th class="text-[#00CA08] text-center">Money Receipt Folder</th>
              </tr>
            </thead>
            <tbody>
              {[...Array(10).keys()].map((item, i) => (
                <tr>
                  <td>2022-05-21</td>
                  <td>Mr. Abul </td>
                  <td>5248678</td>
                  <td>52A1C4</td>
                  <td>Munshiganj Project</td>
                  <td>
                    <span class="text-[#6B2CEA]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#16A085]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#D60000]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#6B2CEA]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#16A085]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#D60000]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#6B2CEA]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#16A085]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#D60000]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#6B2CEA]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#16A085]">5,0000 TK</span>
                  </td>
                  <td>
                    <span class="text-[#D60000]">5,0000 TK</span>
                  </td>

                  <td class="w-[230px]">
                    <div class="h-full flex">
                      <img
                        class="mr-2"
                        src={AddImage}
                        alt=""
                      />
                      <img
                        class="mr-2"
                        src={ViewImg}
                        alt=""
                      />
                      <img
                        class="mr-2"
                        src={ActionImg}
                        alt=""
                      />
                    </div>
                  </td>

                  <td>
                    <img
                      class="mx-auto"
                      src={FolderImg}
                      alt=""
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <Pagination />
      </div>
    </div>
  );
};

export default ClientPaymentInformation;
