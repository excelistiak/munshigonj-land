import React from "react";

const LandPriceInformation = () => {
  return (
    <div>
      <p className="text-[#6B2CEA] font-bold text-[16px] bg-[#F0EAFD] py-1 my-[40px]  text-center">
        Land Price Information
      </p>

      <div className="grid gap-4 xl:grid-cols-5 lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
        <div className="common_input_wraper">
          <label>Total Amount</label>
          <input type="text" placeholder="Amount" />
        </div>
        <div className="common_input_wraper">
          <label>Land Price per Decimal</label>
          <input type="text" placeholder="Amount" />
        </div>
        <div className="common_input_wraper">
          <label>Booking Money</label>
          <input type="text" placeholder="amount" />
        </div>
        <div className="common_input_wraper">
          <label>Down Payment</label>
          <input type="text" placeholder="Amount" />
        </div>
        <div className="common_input_wraper">
          <label>Total Instalment Amount </label>
          <input type="text" placeholder="Amount" />
        </div>
        <div className="common_input_wraper">
          <label>No of Instalment </label>
          <input type="text" placeholder="Amount" />
        </div>
        <div className="common_input_wraper">
          <label>Per month Instalment amount</label>
          <input type="text" placeholder="Amount" />
        </div>
      </div>
    </div>
  );
};

export default LandPriceInformation;
