import React from "react";
import dashboardCardData from "../../asset/constant/dashboardCardData";
import "./dashboard.css";
const Dashbaord = () => {
  return (
    <div id="dashboard">
      <h1 class="text-[#333547] font-[700] text-[24px]">Dashboard</h1>
      <div class="grid xl:grid-cols-4 lg:grid-cols-2 md:grid-cols-1 gap-3">
        {dashboardCardData.map((item) => (
          <figure class="dashboard_card">
            <div className="bg-[#533198] h-[68px] w-[68px] rounded-full flex items-center justify-center">
              <img class="   object-cover" src={item.url} alt="" />
            </div>
            <figure>
              <figcaption class="text-[#000000] lg:max-w-[180px] max-w-[150px] font-[500] text-[16px]">
                {item.title}
              </figcaption>
              <figcaption class="text-[#000000] font-[500] text-[26px] mt-[10px]">
                {item.total}
              </figcaption>
            </figure>
          </figure>
        ))}
      </div>
    </div>
  );
};

export default Dashbaord;
