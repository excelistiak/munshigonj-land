import React from "react";
import Pagination from "../../../shared/pagination/Pagination";
import {
  addCustomerCss,
  commonSearchInput,
  dashboardBodyWraper,
} from "../../../asset/commoncssConstant/CommonCssConstant";
import CommonPrintPdfBtn from "../../../shared/CommonPrintPdfBtn";
import ViewBtn from "../../../asset/images/clientlist/view.png";
import ActionBtn from "../../../asset/images/clientlist/action.png";
const AgentList = () => {
  return (
    <div>
      <h1 class="text-[#333547] font-[700] text-[24px]">Agent List</h1>
      <div className={dashboardBodyWraper}>
        <button className={addCustomerCss}>
          Add Agent{" "}
          <img class="ml-[30px]" src="./image/clientlist/add-user.png" alt="" />
        </button>
        <div class="flex justify-between  flex-wrap gap-3 items-end mt-[20px]">
          <CommonPrintPdfBtn />
          <input
            type="text"
            placeholder="Search"
            className={commonSearchInput}
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start">Agent join Date</th>
                <th class="text-[#00CA08]">Agent Id</th>
                <th class="text-[#00CA08] text-start">Agent Name</th>
                <th class="text-[#00CA08]">Father Name</th>
                <th class="text-[#00CA08] text-start">Mother Name</th>
                <th class="text-[#00CA08] text-start">NID No</th>
                <th class="text-[#00CA08]">Date of Birth</th>
                <th class="text-[#00CA08]">Picture</th>
                <th class="text-[#00CA08]">
                  Educational <br />
                  Background
                </th>
                <th class="text-[#00CA08] text-start">
                  Agent <br />
                  mobile no. 1
                </th>
                <th class="text-[#00CA08]">
                  Agent <br />
                  mobile no. 2
                </th>

                <th class="text-[#00CA08]">Email</th>
                <th class="text-[#00CA08] text-center">
                  Agent Present Address
                </th>
                <th class="text-[#00CA08] text-center">
                  Agent Permanent Address
                </th>
                <th class="text-[#00CA08] text-center">Action Update/ View</th>
              </tr>
            </thead>
            <tbody>
              {[...Array(14).keys()].map((item, i) => (
                <tr>
                  <td>2022-05-21</td>
                  <td>
                    <span class="font-bold">5248678</span>
                  </td>
                  <td>Mr. Abul Kashem </td>
                  <td>Md. Barkat Ullah</td>

                  <td>Mrs. jannatul Ferdous</td>
                  <td>1234567891</td>
                  <td>1978-07-22</td>
                  <td>
                    <span className="font-bold">1091</span>
                  </td>
                  <td>
                    <span className="font-bold">MBA</span>
                  </td>

                  <td>
                    <span class="text-[#0085FC] underline">01756324158</span>
                  </td>
                  <td>
                    <span class="text-[#0085FC] underline">01756324158</span>
                  </td>
                  <td>agent@ gmail.com</td>
                  <td>111/C Moghbazar, Dhaka 1217</td>
                  <td>111/C Moghbazar, Dhaka 1217</td>

                  <td>
                    <span className="flex">
                      <img class="mx-auto" src={ActionBtn} alt="" />
                      <img class="mx-auto" src={ViewBtn} alt="" />
                    </span>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <Pagination />
      </div>
    </div>
  );
};

export default AgentList;
