import React from "react";
import Pagination from "../../../shared/pagination/Pagination";
import {
  commonSearchInput,
  dashboardBodyWraper,
} from "../../../asset/commoncssConstant/CommonCssConstant";
import CommonPrintPdfBtn from "../../../shared/CommonPrintPdfBtn";
import ViewBtn from "../../../asset/images/clientlist/view.png";
import ActionBtn from "../../../asset/images/clientlist/action.png";

const AgentWorkList = () => {
  return (
    <div>
      <h1 class="text-[#333547] font-[700] text-[24px]">Agent Work List</h1>
      <div className={dashboardBodyWraper}>
        {/* <button className={addCustomerCss}>
            Add Customer{" "}
            <img class="ml-[30px]" src="./image/clientlist/add-user.png" alt="" />
          </button> */}
        <div class="flex justify-between  flex-wrap gap-3 items-end mt-[20px]">
          <CommonPrintPdfBtn />
          <input
            type="text"
            placeholder="Search"
            className={commonSearchInput}
          />
        </div>

        <div class="table_responsive mt-[40px]">
          <table>
            <thead>
              <tr>
                <th class="text-[#00CA08] text-start">Client Booking Date</th>
                <th class="text-[#00CA08]">Agent Id</th>
                <th class="text-[#00CA08] text-start">Agent Name</th>
                <th class="text-[#00CA08]">
                  Agent Present <br />
                  Address
                </th>
                <th class="text-[#00CA08] text-start">
                  Agent <br />
                  mobile no. 1
                </th>
                <th class="text-[#00CA08] text-start">
                  Agent <br />
                  mobile no. 2
                </th>
                <th class="text-[#00CA08]">
                  Customer’s <br />
                  ID
                </th>
                <th class="text-[#00CA08]">
                  Customer’s <br />
                  Name
                </th>
                <th class="text-[#00CA08]">
                  Customer <br />
                  mobile no.
                </th>
                <th class="text-[#00CA08] text-start">Project Name</th>
                <th class="text-[#00CA08]">
                  File/ <br />
                  Plot No.
                </th>

                <th class="text-[#00CA08]">Total Price</th>
                <th class="text-[#00CA08] text-center">Agent Money</th>
                <th class="text-[#00CA08] text-center">Edit / View</th>
              </tr>
            </thead>
            <tbody>
              {[...Array(14).keys()].map((item, i) => (
                <tr>
                  <td>2022-05-21</td>
                  <td>
                    <span class="font-bold">5248678</span>
                  </td>
                  <td>Mr. Abul Kashem </td>
                  <td>111/C Moghbazar, Dhaka 1217</td>
                  <td>
                    <span class="text-[#0085FC] underline">01756324158</span>
                  </td>
                  <td>
                    <span class="text-[#0085FC] underline">01756324158</span>
                  </td>
                  <td>
                    <span class="font-bold">5248678</span>
                  </td>

                  <td>Mr. Abul </td>
                  <td>
                    <span class="text-[#0085FC] underline">01756324158</span>
                  </td>
                  <td>Munshiganj Project</td>
                  <td>
                    <span class="font-bold">52A1C4</span>
                  </td>
                  <td>
                    <span class="font-bold">7000000</span>
                  </td>
                  <td>
                    <span class="font-bold">15000</span>
                  </td>

                  <td>
                    <span className="flex">
                      <img class="mx-auto" src={ActionBtn} alt="" />
                      <img class="mx-auto" src={ViewBtn} alt="" />
                    </span>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <Pagination />
      </div>
    </div>
  );
};

export default AgentWorkList;
