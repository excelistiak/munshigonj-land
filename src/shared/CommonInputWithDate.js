import React from "react";

const CommonInputWithDate = (props) => {
  const { label } = props;
  return (
    <div className="common_date_with_input">
      <div className="label_wraper">
        <label>{label}</label>
        <label>start date</label>
      </div>
      <div className="text_date_wraper">
        <input name="text" {...props} type="text" />
        <input name="date" {...props} type="date" />
      </div>
    </div>
  );
};

export default CommonInputWithDate;
