import { Route, Routes } from "react-router-dom";
import "./App.css";
import './style/common.css'
import MainLayout from "./layout/MainLayout";
import Dashbaord from "./modules/Dashboard/Dashbaord";
import ClientList from "./modules/clientList/ClientList";
import ClientPriceInformation from "./modules/clientpriceinformation/ClientPriceInformation";
import ClientPaymentInformation from './modules/clientpaymentinformation/ClientPaymentInformation';
import ClientLandDescription from "./modules/clientlanddescription/ClientLandDescription";
import AgentList from "./modules/agentInformation/AgentList/AgentList";
import AgentWorkList from "./modules/agentInformation/AgentWorkList/AgentWorkList";
import AgentPaymentList from "./modules/agentInformation/AgentPaymentList/AgentPaymentList";
import RegistryCompleteLandInformation from "./modules/projectLandDetails/RegistryCompleteLandInformation/RegistryCompleteLandInformation";
import AddClient from "./modules/clientList/addClient/AddClient";
import LandSellerInformation from "./modules/landSellerInformation/LandSellerInformation";
import LandSellerPaymentInformation from "./modules/landsellerpaymentInformation/LandSellerPaymentInformation";
import MouzaMapLandPurchasefromLandOwner from "./modules/MouzaMapLandPurchasefromLandOwner/MouzaMapLandPurchasefromLandOwner";
import MouzaMapLandSelltoCustomer from "./modules/MouzaMapLandSelltoCustomer/MouzaMapLandSelltoCustomer";
import ClientView from "./modules/clientList/viewClient/ClientView";
import PaymentShedule from "./modules/clientpriceinformation/paymentshedule/PaymentShedule";

function App() {

  return (
    <div className="bg-white max-h-screen w-screen">
      <Routes>
        <Route path="/" element={<MainLayout />}>
          <Route index  element={<Dashbaord />} />
          <Route path="dashboard/client-list" element={<ClientList />} />
          <Route path="dashboard/client-list/view/:id" element={<ClientView />} />
          <Route path="dashboard/client-list/add-client" element={<AddClient />} />
          <Route
            path="dashboard/client-price-information"
            element={<ClientPriceInformation />}
          />
          <Route
            path="dashboard/client-price-information/payment-shedule/:id"
            element={<PaymentShedule />}
          />
          <Route
            path="dashboard/client-payment-information"
            element={<ClientPaymentInformation />}
          />
          <Route
            path="dashboard/client-land-description"
            element={<ClientLandDescription />}
          />
          <Route
            path="dashboard/registry-complete-land-information"
            element={<RegistryCompleteLandInformation />}
          />
          <Route
            path="dashboard/land-seller-information"
            element={<LandSellerInformation />}
          />
          <Route
            path="dashboard/land-seller-payment-information"
            element={<LandSellerPaymentInformation />}
          />
          <Route
            path="dashboard/mouzamap-landpurchase-fromlandowner"
            element={<MouzaMapLandPurchasefromLandOwner />}
          />
          <Route
            path="dashboard/mouzamap-landsell-tocustomer"
            element={<MouzaMapLandSelltoCustomer />}
          />
          <Route
            path="dashboard/agent-information/agent-list"
            element={<AgentList />}
          />
          <Route
            path="dashboard/agent-information/agent-work-list"
            element={<AgentWorkList />}
          />
          <Route
            path="dashboard/agent-information/agent-payment-list"
            element={<AgentPaymentList />}
          />
          <Route path="*" element={<h1>Page not exist</h1>} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
